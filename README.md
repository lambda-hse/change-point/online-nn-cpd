# online-nn-cpd

This repository contains tests and implementations of change point detection algorithms based on direct density ratio estimation from the following article:

- Mikhail Hushchyn, Kenenbek Arzymatov and Denis Derkach. “Online Neural Networks for Change-Point Detection.” ArXiv abs/2010.01388 (2020). [[arxiv]](https://doi.org/10.48550/arXiv.2010.01388)

# Python library

Algorithms are implemented in our python library `roerich` https://github.com/HSE-LAMBDA/roerich.
